# Router Reviews

Router Reviews is a website that allows users to upload router models from a variety of vendors and review them for the public to see. Users are able to rate routers using a 1-5 star rating and provide a detailed review of the product which can be upvoted or downvoted by the community. Users are able to follow each other in order to gain insight into other reviews that have been made by a given user.

<strong>PLEASE NOTE:</strong> The website is a University project in development stages and is not being hosted.

## Moderator Credentials
Reviews can be moderated and routers can be deleted by the moderator account. The option to register for a moderator account has not yet been implemented so the default credentials are as follows:

```
Username: moderator@example.com
Password: 123456
```

## Run Project
1. Download Dependencies

```
composer install
```

2. Create a database file called `database.sqlite` in `/database` directory


3. Migrate and Seed the database with supplied test data

```
php artisan migrate:refresh --seed
```

4. Create a symbolic link from storage directory to public directory

```
php artisan storage:link
```

5. Start the Server on port 8000

```
php artisan serve
```

## Documentation
Further documentation can be found at http://localhost:8000/documentation

