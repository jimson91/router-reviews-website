<div id="register-backdrop" class="modal-backdrop">
    <div class="fixed-action-btn modal-close">
        <a class="btn-floating btn-large red modal-exit" data-target="#register-modal" data-modal="#register-backdrop">
            <i class="material-icons">close</i>
        </a>
    </div>
    <div class="backdrop-background"></div>
</div>
<div id="register-modal" class="custom-modal">
    <div class="row custom-row">
        <div class="col s12 m8 l5 custom-col">
            <form id="register-form" class="form-horizontal">
                <div class="card custom-card">
                    <div class="indigo darken-1 white-text" style="padding:10px;">
                        <h5 class="center">Register</h5>
                    </div>
                    <div class="card-content">
                        <p class="center">Join to our community now !</p>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="input-field col s12 m6 l6">
                                <i class="material-icons prefix">perm_identity</i>
                                <label for="first_name">First Name</label>
                                <input id="first_name" type="text" name="first_name" class="validate" value="{{old('first_name')}}" required>
                                <span class="helper-text" id="helper-first_name" data-error=""></span>
                            </div>
                            <div class="input-field col s12 m6 l6">
                                <i class="material-icons prefix">perm_identity</i>
                                <label for="last_name">Last Name</label>
                                <input id="last_name" type="text" name="last_name" class="validate" value="{{old('last_name')}}" required>
                                <span class="helper-text" id="helper-last_name" data-error=""></span>
                            </div>
                            <div class="input-field col s12 m6 l6">
                                <i class="material-icons prefix">date_range</i>
                                <label for="date_birth">Date of Birth</label>
                                <input id="date_birth" type="text" name="date_birth" class="datepicker validate" value="{{old('date_birth')}}" required>
                                <span class="helper-text" id="helper-date_birth" data-error=""></span>
                            </div>
                            <div class="input-field col s12 m6 l6">
                                <i class="material-icons prefix">email</i>
                                <label for="email">Email</label>
                                <input id="register_email" type="email" name="email" value="{{old('email')}}" class="validate" required>
                                <span class="helper-text" id="helper-email" data-error=""></span>
                            </div>
                            <div class="input-field col s12 m6 l6">
                                <i class="material-icons prefix">security</i>
                                <label for="password">Password</label>
                                <input id="register_password" type="password" name="password" class="validate" required>
                                <span class="helper-text" id="helper-password" data-error=""></span>
                            </div>
                            <div class="input-field col s12 m6 l6">
                                <i class="material-icons prefix">security</i>
                                <label for="password_confirmation">Confirm Password</label>
                                <input id="register_password_confirm" type="password" name="password_confirmation" class="validate" required>
                                <span class="helper-text" data-error=""></span>
                            </div>
                        </div>
                        <input type="submit" value="Register" class="btn left col s12">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
