<div id="login-backdrop" class="modal-backdrop">
    <div class="fixed-action-btn modal-close">
        <a class="btn-floating btn-large red modal-exit" data-target="#login-modal" data-modal="#login-backdrop">
            <i class="material-icons">close</i>
        </a>
    </div>
    <div class="backdrop-background"></div>
</div>
<div id="login-modal" class="custom-modal">
    <div class="row custom-row">
        <div class="col s11 m6 l4 custom-col">
            <form id="login-form" class="form-horizontal">
                <div class="card custom-card">
                    <div class="indigo darken-1 white-text" style="padding:10px;">
                        <h5 class="center">Login</h5>
                    </div>
                    <div class="card-content">
                        <p class="center">Access Your Account</p>

                        <div class="input-field">
                            <i class="material-icons prefix">email</i>
                            <label for="email">Enter Email</label>
                            <input id="login-email" name="email" type="text" class="validate" required>
                            <span class="helper-text" id="helper-email" data-error=""></span>
                        </div>
                        <div class="input-field">
                            <i class="material-icons prefix">security</i>
                            <label for="password">Enter Password</label>
                            <input id="login-password" name="password" type="password" class="validate" required>
                            <span class="helper-text" id="helper-password" data-error=""></span>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" value="Sign In" class="btn col s12 m5 right">Sign In</button>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
