@if(Auth::check())
<!-- Modal Structure -->
<div id="review_modal" class="modal modal-fixed-footer">
    <form method="POST" action="/review">
        <div class="modal-content">
            <h4 class="text-center">Leave a review for the {{$vendor->name}} - {{$product->model}}
            </h4>
            <!-- Form for adding new reviews -->
            <input type="hidden" name="product_id" value="{{$product->id}}">
            <input type="hidden" name="user_id" value="{{ Auth::id() }}">
            {{csrf_field()}}
            <div class="input-field col s12">
                <i class="material-icons prefix">star</i>
                <select name="rating" required>
                    @if (count($errors) == 0)
                    <option value="" disable selected>Select</option>
                    @else
                    <option value="{{old('rating')}}" selected>{{ old('rating') }}</option>
                    @endif
                    <option value="1">&#9733;</option>
                    <option value="2">&#9733;&#9733;</option>
                    <option value="3">&#9733;&#9733;&#9733;</option>
                    <option value="4">&#9733;&#9733;&#9733;&#9733;</option>
                    <option value="5">&#9733;&#9733;&#9733;&#9733;&#9733;</option>
                </select>
                <label for="rating">Rating:</label>
            </div>
            <div class="input-field col s12">
                @if ($errors->has('text'))
                <i class="material-icons prefix">rate_review</i>
                <label for="text">Review</label>
                <textarea rows="8" class="invalid materialize-textarea" name="text" required>{{ old('text') }}</textarea>
                <span class="helper-text" data-error="{{$errors->first('text')}}"></span>
                @else
                <i class="material-icons prefix">rate_review</i>
                <label for="text">Review</label>
                <textarea rows="8" class="materialize-textarea" name="text" required></textarea>
                @endif
            </div>
            @if($errors->has('duplicate'))
            <span class="red-text">{{$errors->first('duplicate')}}</span>
            @endif
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect btn-flat">
                <i class="material-icons right">close</i>
                close
            </a>
            <button type="submit" class="waves-effect btn-flat">
                <i class="material-icons right">send</i>
                Submit
            </button>
        </div>
    </form>
</div>
@endif
