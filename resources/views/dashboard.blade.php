@extends('layouts.master')

@section('title')
Dashboard | Router Reviews
@endsection

@section('description')
Review your account, configure personal settings or check your review feed
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12 m12 l10 offset-l1">
            <div class="card hoverable">
                <div class="card-content" style="max-height: 100vh; overflow-y: auto;">
                    <div class="row">
                        <div class="col s12 m12 l6">
                            <div class="card-title">Account</div>
                            <div class="divider"></div>
                            @if(Auth::user()->moderator == 1)
                            <div class="card-panel red lighten-3 white-text">
                                <i class="material-icons left">account_circle</i>
                                You're logged in as MODERATOR
                            </div>
                            @else
                            <div class="card-panel blue lighten-2 white-text">
                                <i class="material-icons left">account_circle</i>
                                You're logged in as {{Auth::user()->getFullName()}}
                            </div>
                            @endif
                            <table class="striped centered responsive-table">
                                <thead>
                                    <tr>
                                        <td></td>
                                        <th>Full Name</th>
                                        <th>Date of Birth</th>
                                        <th>Account Type</th>
                                        <th>Account Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><img src="{{asset(Auth::user()->avatar)}}" alt="user_avatar" class="circle" width="50">
                                        </td>
                                        <td>{{Auth::user()->getFullName()}}</td>
                                        <td>{{date('d-m-Y', strtotime(Auth::user()->date_birth))}}</td>
                                        <td>Moderator</td>
                                        <td>{{Auth::user()->created_at}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <ul class="collapsible">
                                <li>
                                    <div class="collapsible-header"><i class="material-icons">image</i>Change Avatar</div>
                                    <div class="collapsible-body">
                                        <form method="POST" action="/avatar" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>Upload</span>
                                                    <input type="file" name="image" onchange="form.submit()">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col s12 m12 l6">
                            <div class="card-title">Following {{count($followers)}} Users</div>
                            <div class="divider"></div>
                            @if (Auth::user()->followers->count() > 0)
                            <table class="highlight centered responsive-table">
                                <thead>
                                    <tr>
                                        <td></td>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (Auth::user()->followers as $follower)
                                    <form method="POST" action="/unfollow">
                                        <tr>
                                            <td><img src="{{asset($follower->avatar)}}" class="circle" alt="profile_image" width="50"></td>
                                            <td>{{ $follower->getFullName() }}</td>
                                            <td>{{ $follower->email }}</td>
                                            <td><a href="unfollow/{{$follower->id}}">Unfollow</a></td>
                                        </tr>
                                    </form>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="card-panel yellow lighten-4 grey-text center">
                                <p>You have not followed any users yet</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m12 l10 offset-l1">
            <div class="card">
                <div class="card-content" style="overflow-y: auto; height:100vh;">
                    <div class="card-title">Review Feed</div>
                    <div class="divider"></div>
                    <ul class="tabs tabs-fixed-width">
                        <li class="tab"><a class="active" href="#followers">Followers</a></li>
                        <li class="tab"><a href="#user_reviews">My Reviews</a></li>
                    </ul>
                    <div id="followers">
                        @if ($followers->count() > 0)
                        @foreach ($feed as $reviews)
                        @foreach ($reviews as $review)
                        <ul class="collection hoverable with-header col s12 l6 match-height">
                            <li class="collection-header">
                                <span class="title">
                                    <strong>Posted by {{$review->user->getFullName()}}</strong>
                                    ({{$review->created_at}})</span>
                                <a class="right" href="/product/{{$review->product_id}}">Show Product</a>
                            </li>
                            <li class="collection-item avatar">
                                <img src="{{url($review->user->avatar)}}" class="circle responsive-img" />
                                <p>{{$review->text}}</p>
                            </li>
                        </ul>
                        @endforeach
                        @endforeach
                        @else
                        <div class="card-panel yellow lighten-4 grey-text center">
                            <p>You have not followed any users yet</p>
                        </div>
                        @endif
                    </div>
                    <div id="user_reviews" style="height: 100%;">
                        @if ($user_reviews->count() > 0)
                        @foreach ($user_reviews as $review)
                        <ul class="collection hoverable with-header col s12 l6">
                            <li class="collection-header">
                                <span class="title"><strong>Posted by {{$review->user->getFullName()}}</strong>
                                    ({{$review->created_at}})</span>
                                <a class="right" href="/product/{{$review->product_id}}">Show Product</a>
                            </li>
                            <li class="collection-item avatar">
                                <img src="{{url($review->user->avatar)}}" class="circle responsive-img" />
                                <p>{{$review->text}}</p>
                            </li>
                        </ul>
                        @endforeach
                        @else
                        <div class="card-panel yellow lighten-4 grey-text">
                            <p>You have not made any reviews</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
