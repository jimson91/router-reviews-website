<!DOCTYPE html>
<html>

<head>
    <link rel="shortcut icon" href="{{asset('favicon.png')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>
<!--No JavaScript Alert -->
<noscript>
    <div>This page needs JavaScript enabled to work properly</div>
</noscript>

<body>
    <div class="parallax-container">
        <div class="parallax"><img src="{{asset('home.jpg')}}"></div>
        <div class="center">
            <nav>
                <div class="nav-wrapper">
                    <li>
                        <a class="brand-logo center">
                            <i class="material-icons">router</i>
                            Router Reviews
                        </a>
                    </li>
                </div>
            </nav>
        </div>
    </div>
    <div class="section white">
        <div class="row container">
            <h2 class="header">Router Reviews</h2>
            <a href="/product" class="btn">Browse Selection</a>
            <p class="grey-text text-darken-3 lighten-3">Browse through a large selection of routers and see community reviews</p>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax"><img src="{{asset('home.jpg')}}"></div>
    </div>
</body>
<script>
    $(document).ready(function() {
        $('.parallax').parallax();
    });

</script>

</html>
