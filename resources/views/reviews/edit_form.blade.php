@extends('layouts.master')

@section('title')
Edit Review
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12 m12 l8 offset-l2">
            <div class="card">
                <form method="POST" action="/review/{{$review->id}}">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                    <input type="hidden" name="product_id" value="{{$review->product_id}}">
                    <div class="card-content">
                        <h2 class="card-title">Edit Review</h2>
                        <div class="row">
                            <div class="input-field col s6 left">
                                <i class="material-icons prefix">star</i>
                                <select class="icons" name="rating">
                                    <option value="{{$review->rating}}" selected hidden>{{$review->rating}}</option>
                                    <option value="1">&#9733;</option>
                                    <option value="2">&#9733;&#9733;</option>
                                    <option value="3">&#9733;&#9733;&#9733;</option>
                                    <option value="4">&#9733;&#9733;&#9733;&#9733;</option>
                                    <option value="5">&#9733;&#9733;&#9733;&#9733;&#9733;</option>
                                </select>
                                <label for="rating">Rating</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                @if ($errors->has('text'))
                                <i class="material-icons prefix">rate_review</i>
                                <label for="text">Review</label>
                                <textarea rows="8" class="invalid materialize-textarea" name="text" required>{{ old('text') }}</textarea>
                                <span class="helper-text" data-error="{{$errors->first('text')}}"></span>
                                @else
                                <i class="material-icons prefix">rate_review</i>
                                <label for="text">Review</label>
                                <textarea rows="8" class="materialize-textarea" name="text" required>{{$review->text}}</textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <input type="reset" class="btn red" value="Reset">
                        <input type="submit" class="btn blue" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
