<div class="card">
    <div class="card-content">
        @if($reviews->total() > 0)
        <div class="row">
            <div class="col s12 m10 l8 offset-m1 offset-l2">
                <div class="card">
                    <ul class="collection with-header hoverable">
                        <li class="collection-header deep-purple darken-3 white-text">
                            <h5 class="center">{{$product->model}} REVIEWS</h5>
                        </li>
                        <li class="collection-item">
                            <div class="col s12 m4 center">
                                <br>
                                <div class="card-title" style="font-size: 45px !important;">{{$product->avgRating()}}
                                </div>
                                @for ($i = 0; $i < round($product->avgRating()); $i++)
                                    <span class="star star-gold">&#9733;</span>
                                    @endfor
                                    @for ($i = 5; $i > round($product->avgRating()); $i--)
                                    <span class="star">&#9734;</span>
                                    @endfor
                                    <div class="">
                                        <span><i class="material-icons">person</i> {{$reviews->total()}} total</span>
                                    </div>
                            </div>
                            <div class="col s12 m8">
                                <div class="chart-container" style="position: relative;">
                                    <canvas id="review_graph"></canvas>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <nav>
            <div class="nav-wrapper deep-purple darken-3 white-text">
                <ul class="left">
                    <li>
                        <a>Displaying {{$reviews->count()}} of {{$reviews->total()}} results</a>
                    </li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li>
                        @sortablelink('created_at', 'Sort by Date', ['filter' => 'active, visible'])
                    </li>
                    <li>
                        @sortablelink('rating', 'Sort by Rating', ['filter' => 'active, visible'])
                    </li>
                </ul>
                <a class='right dropdown-trigger btn-flat white-text hide-on-large-only' data-target='sort_dropdown'>
                    <i class="material-icons">sort</i>
                </a>
                <ul id='sort_dropdown' class='dropdown-content'>
                    <li>
                        @sortablelink('created_at', 'Sort by Date', ['filter' => 'active, visible'])
                    </li>
                    <li class="divider"></li>
                    <li>
                        @sortablelink('rating', 'Sort by Rating', ['filter' => 'active, visible'])
                    </li>
                </ul>
            </div>
        </nav>
        <!--Review Cards -->
        <div class="row">
            @foreach($reviews as $review)
            <div class="col s12 m6 l6">
                <ul class="collection hoverable">
                    @if($review->user_id == Auth::id() || Auth::check() && Auth::user()->moderator)
                    <li class="collection-item">
                        <form method="POST" action="/review/{{$review->id}}">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <a class="btn-floating btn-small waves-effect blue tooltipped" data-position="top" data-tooltip="Edit " href="/review/{{$review->id}}/edit">
                                <i class="material-icons">edit</i>
                            </a>
                            <button class="btn-floating btn-small waves-effect blue tooltipped" data-position="top" data-tooltip="Delete" href="javascript:void(0)">
                                <i class="material-icons">delete</i>
                            </button>
                        </form>
                    </li>
                    @endif
                    <li class="collection-item avatar match-height" style="overflow-y: auto;">
                        <img src="{{asset($review->user->avatar)}}" alt="{{$review->user->getFullName()}}" class="circle responsive-img">
                        <div class="left">
                            <p class="title"><strong>{{$review->user->getFullName()}}</strong></p>
                            <p>{{$review->created_at}}</p>
                        </div>
                        <div class="right">
                            @for ($i = 0; $i < $review->rating; $i++)
                                <span class="star star-gold">&#9733;</span>
                            @endfor
                            @for ($i = 5; $i > $review->rating; $i--)
                                <span class="star">&#9734;</span>
                            @endfor
                        </div>
                        <br><br>
                        <div class="row"></div>
                        <p>{{$review->text}}</p>
                    </li>
                    <li class="collection-item options">
                        <a class="waves-effect waves-light btn blue trigger-modal" id="like" @guest href="#login-modal" data-target="#login-modal" data-modal="#login-backdrop" @endguest>
                            <i class="material-icons left">thumb_up</i>
                            {{$review->likes->count()}}
                        </a>
                        <a class="waves-effect waves-light btn red trigger-modal" id="dislike" @guest href="#login-modal" data-target="#login-modal" data-modal="#login-backdrop" @endguest>
                            <i class="material-icons left">thumb_down</i>
                            {{$review->dislikes->count()}}
                        </a>
                        <a class="waves-effect waves-light btn right trigger-modal" id="follow" @guest href="#login-modal" data-target="#login-modal" data-modal="#login-backdrop" @endguest>
                            <i class="material-icons left">person_add</i>
                            Follow
                        </a>
                        <input type="hidden" id="review_id" value="{{$review->id}}">
                        <input type="hidden" id="user_id" value="{{$review->user_id}}">
                    </li>
                </ul>
            </div>
            @endforeach
        </div>
        {{$reviews->links("pagination::bootstrap-4")}}
        @else
        <div class="card-panel red lighten-2 white-text">
            <p>No reviews. Be the first to leave a review</p>
        </div>
        @endif
    </div>
</div>
