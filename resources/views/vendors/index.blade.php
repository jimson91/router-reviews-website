@extends('layouts.master')

@section('title')
Search Vendor | Router Reviews
@endsection

@section('description')
Search Vendor to find a community reviews for a specific router
@endsection

@section('keywords')
search, vendor, router
@endsection

@section('content')
<div class="row">
    <div class="col s12 m12 l4">
        <div class="card deep-purple-text">
            <div class="card-content">
                <div class="valign-wrapper">
                    <img class="index-title" src="{{asset('favicon.png')}}" width=70>
                    <h4 class="index-title">Search Vendor</h4>
                </div>
                <div class="divider"></div>
                <br>
                <ul class="collapsible">
                    <li class="active">
                        <div class="collapsible-header"><i class="material-icons">sort</i>Sort Options</div>
                        <div class="collapsible-body">
                            <ul class="collection">
                                @sortablelink('name', 'Vendor (A-Z)', ['filter' => 'active, visible'], ['class' => 'collection-item deep-purple-text'])
                                @sortablelink('products_count', 'No of Routers', ['filter' => 'active, visible'], ['class' => 'collection-item deep-purple-text'])
                            </ul>
                        </div>
                    </li>
                </ul>
                @if($vendors->total() > 10)
                {{$vendors->links("pagination::bootstrap-4")}}
                @endif
            </div>
        </div>
    </div>
    <div class="col s12 m12 l8">
        <div class="card">
            <div class="card-content">
                <h6 class="card-title">Displaying {{$vendors->count()}} of {{$vendors->total()}} results</h6>
                @if($vendors->total() > 0)
                <div class="row">
                    @foreach($vendors as $vendor)
                    <div class="col s12 m6 l4">
                        <div class="card hoverable">
                            <div class="card-content match-height">
                                <a href="/vendor/{{$vendor->id}}"><img src="{{$vendor->logo}}" alt="{{$vendor->name}}" class="responsive-img"></a>
                            </div>
                            <div class="card-action">
                                Routers: {{$vendor->products_count}}
                            </div>
                            <a class="btn-floating halfway-fab waves-effect red tooltipped" href="/vendor/{{$vendor->id}}" data-position="top" data-tooltip="Show"><i class="material-icons">arrow_forward</i></a>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <div class="card-panel red lighten-2 white-text">
                    <div class="card-title">No records found</div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
