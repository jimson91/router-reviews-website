<div class="card">
    <div class="card-content">
        <div class="row">
            <div class="col s12 m12 l6">
                <img src="{{asset($vendor->logo)}}" alt="router vendor" width=200 class="responsive-img">
                <div class="page-heading">
                    <h4 style="margin: 0;">{{$product->model}}</h4>
                    <div class="stars right">
                        @for ($i = 0; $i < round($product->avgRating()); $i++)
                            <span class="star star-gold">&#9733;</span>
                            @endfor
                            @for ($i = 5; $i > round($product->avgRating()); $i--)
                            <span class="star">&#9734;</span>
                            @endfor
                    </div>
                </div>
                <br>
                <div class="divider"></div>
            </div>
        </div>
        <div class="row valign-wrapper">
            <div class="col s12 m12 l6">
                <div class="card-title">Specifications</div>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <ul class="collection match-height">
                            <li class="collection-item grey lighten-4">
                                <strong>
                                    <i class="material-icons left">date_range</i>
                                    Release Date
                                </strong>
                                <p>{{$product->release_date}}</p>
                            </li>
                            <li class="collection-item">
                                <strong>
                                    <i class="material-icons left">network_wifi</i>
                                    Wireless Frenquencies
                                </strong>
                                <p>{{$product->wireless_frequency}}</p>
                            </li>
                            <li class="collection-item grey lighten-4">
                                <strong>
                                    <i class="material-icons left">router</i>
                                    Wireless Standard
                                </strong>
                                <p>{{$product->wireless_standard}}</p>
                            </li>
                            <li class="collection-item">
                                <strong>
                                    <i class="material-icons left">security</i>
                                    Wireless Security
                                </strong>
                                <p>{{$product->wireless_security}}</p>
                            </li>
                            <li class="collection-item grey lighten-4">
                                <strong>
                                    <i class="material-icons left">file_download</i>
                                    Wireless Speed
                                </strong>
                                <p>{{$product->wireless_speed}}</p>
                            </li>
                        </ul>
                    </div>
                    <div class="col s12 m6 l6">
                        <ul class="collection match-height">
                            <li class="collection-item">
                                <strong>
                                    <i class="material-icons left">settings_input_antenna</i>
                                    Number of Antennas
                                </strong>
                                <p>{{$product->antenna}}</p>
                            </li>
                            <li class="collection-item grey lighten-4">
                                <strong>
                                    <i class="material-icons left">memory</i>
                                    Memory
                                </strong>
                                <p>{{$product->memory}}</p>
                            </li>
                            <li class="collection-item">
                                <strong>
                                    <i class="material-icons left">usb</i>
                                    USB Sharing Support
                                </strong>
                                <p>{{$product->usb_sharing}}</p>
                            </li>
                            <li class="collection-item grey lighten-4">
                                <strong>
                                    <i class="material-icons left">security</i>
                                    Firewall
                                </strong>
                                <p>{{$product->firewall}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l6">
                <div class="slider">
                    <ul class="slides">
                        @if($images->count() > 0)
                        @foreach($images as $image)
                        <li>
                            <img src="{{asset($image->path)}}" alt="{{$product->model}}">
                        </li>
                        @endforeach
                        @else
                        <li>
                            <img src="{{asset('no_image_available.png')}}" alt="no image available">
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @auth
    <div class="card-action">
        <div class="row valign-wrapper" style="margin:0;">
            <div class="col s12 m6 l6">
                @if (Auth::user()->moderator)
                <form method="POST" action="/product/{{$product->id}}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a class="waves-effect btn indigo darken-1" href="/product/{{$product->id}}/edit" role="button">
                        <i class="material-icons left">edit</i>
                        Edit
                    </a>
                    <button class="waves-effect btn red darken-1" type="submit">
                        <i class="material-icons left">delete</i>
                        Delete
                    </button>
                </form>
                @endif
            </div>
            <div class="col s12 m6 l6">
                <ul class="collapsible">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">image</i>Upload Image
                        </div>
                        <div class="collapsible-body">
                            <form method="POST" action="/image" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                <div class="file-field input-field">
                                    <div class="btn-small waves-effect">
                                        <i class="material-icons left">image</i>
                                        Upload Image
                                        <input type="file" name="image" onchange="form.submit()">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                        <label>Choose file</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @endauth
</div>
