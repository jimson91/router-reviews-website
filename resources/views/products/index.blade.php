@extends('layouts.master')

@section('title')
Search Router | Router Reviews
@endsection

@section('description')
Search routers for detailed description and commmunity reviews
@endsection

@section('keywords')
search, routers, model, reviews, rating
@endsection

@section('content')
<div class="row">
    <div class="col s12 m12 l4">
        <div class="card deep-purple-text">
            <div class="card-content">
                <div class="valign-wrapper">
                    <img class="index-title" src="{{asset('favicon.png')}}" width=70>
                    <h4 class="index-title">Search Routers</h4>
                </div>
                <div class="divider"></div>
                <br>
                <div class="row">
                    <div class="col s12">
                        <form method="GET" action="/search">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">search</i>
                                <input type="text" id="autocomplete-input" class="autocomplete" name="term" required>
                                <label for="search">Search Model</label>
                            </div>
                        </form>
                    </div>
                </div>
                <ul class="collapsible">
                    <li class="active">
                        <div class="collapsible-header"><i class="material-icons">sort</i>Sort Options</div>
                        <div class="collapsible-body">
                            <ul class="collection">
                                @sortablelink('model', 'Model (A-Z)', ['filter' => 'active, visible'], ['class' => 'collection-item deep-purple-text'])
                                @sortablelink('vendor_id', 'Vendor (A-Z)', ['filter' => 'active, visible'], ['class' => 'collection-item deep-purple-text'])
                                @sortablelink('reviews_count', 'No. Of Reviews', ['filter' => 'active, visible'], ['class' => 'collection-item deep-purple-text'])
                                @sortablelink('release_date', 'Release Date', ['filter' => 'active, visible'], ['class' => 'collection-item deep-purple-text'])
                            </ul>
                        </div>
                    </li>
                </ul>
                @if($products->total() > 10)
                {{$products->links("pagination::bootstrap-4")}}
                @endif
            </div>
        </div>
    </div>
    <div class="col s12 m12 l8">
        @if($products->total() > 0)
        <div class="card">
            <div class="card-content">
                <h6 class="card-title">Displaying {{$products->count()}} of {{$products->total()}} results</h6>
                <div class="row">
                    @foreach($products as $product)
                    <div class="col s12 m6 l3">
                        <div class="card small hoverable">
                            <div class="card-image">
                                <a href="/product/{{$product->id}}">
                                    <img alt="{{$product->model}}" @if(count($product->images) > 0)
                                    src="{{asset($product->images[0]->path)}}" @else
                                    src="{{asset('no_image_available.png')}}" @endif>
                                    <span class="card-title product-card-title">{{$product->model}}</span>
                                </a>
                            </div>
                            <div class="card-content">
                                <p><img width="100" src="{{asset($product->vendor->logo)}}" alt="{{$product->vendor->name}}">
                                </p>
                            </div>
                            <div class="card-action center">
                                @for ($i = 0; $i < round($product->avgRating()); $i++)
                                    <span class="star star-gold">&#9733;</span>
                                    @endfor
                                    @for ($i = 5; $i > round($product->avgRating()); $i--)
                                    <span class="star">&#9734;</span>
                                    @endfor
                            </div>
                            <a class="btn-floating halfway-fab waves-effect red tooltipped" href="/product/{{$product->id}}" data-position="top" data-tooltip="Show {{$product->model}}">
                                <i class="material-icons">arrow_forward</i>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @else
        <div class="card-panel red lighten-2 white-text">
            <div class="card-title">No records found</div>
        </div>
        @endif
    </div>
</div>
@if(Auth::check() && Auth::user()->moderator)
<div class="fixed-action-btn">
    <a class="right btn-floating btn-large waves-effect waves-light red tooltipped" data-position="top" data-tooltip="Add New Model" href="/product/create">
        <i class="material-icons">add</i>
    </a>
</div>
@endif
@include('scripts.autocomplete')
@endsection
