@extends('layouts.master')

@section('title')
{{$product->model}} | Router Reviews
@endsection

@section('description')
{{$product->description}}
@endsection

@section('keywords')
{{$product->model}}, review, specifications, rating
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12 m12 l10 offset-l1">
            @include('products.specs-card')
        </div>
    </div>
</div>
<div id="reviews" class="container">
    <div class="fixed-action-btn">
        <a class="right btn-floating btn-large waves-effect red tooltipped modal-trigger trigger-modal" @auth href="#review_modal" @else data-target="#login-modal" data-modal="#login-backdrop" @endauth data-position="top" data-tooltip="Leave Review">
            <i class="material-icons left">rate_review</i>
        </a>
    </div>
    @include('modals.create_review')
</div>
</div>
<div class="row">
    <div class="col s12 m12 l10 offset-l1">
        @include('reviews.show')
    </div>
</div>
</div>
@include('scripts.star-rating')
@auth
@include('scripts.review_actions')
@if($errors->has('text') || $errors->has('rating') || $errors->has('duplicate'))
<script type="text/javascript">
    $(document).ready(function() {
        $('#review_modal').modal('open');
    });
</script>
@endif
@endauth
@endsection
