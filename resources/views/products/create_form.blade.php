@extends('layouts.master')

@section('title')
Add Router | Router Reviews
@endsection

@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <form method="POST" action="/product">
                {{csrf_field()}}
                <div class="card-content">
                    <h4 class="card-title">Add Router</h4>
                    <div class="row">
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">edit</i>
                            @if ($errors->has('model'))
                            <label for="model">Model</label>
                            <input class="invalid" type="text" name="model" value="{{ old('model') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('model')}}"></span>
                            @else
                            <input class="validate" type="text" name="model" value="{{ old('model') }}" required>
                            <label for="model">Model</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <select name="vendor" required>
                                <option value="" disabled selected>Choose Vendor</option>
                                @foreach ($vendors as $vendor)
                                @if ($vendor->id === old('vendor'))
                                <option value="{{$vendor->id}}" selected="selected">
                                    {{$vendor->name}}
                                </option>
                                @else
                                <option value="{{$vendor->id}}"> {{$vendor->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            <label for="vendor">Vendor</label>
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">router</i>
                            @if ($errors->has('wireless_standard'))
                            <label for="wireless_standard">Wireless Standard</label>
                            <input class="invalid" type="text" name="wireless_standard" value="{{ old('wireless_standard') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('wireless_standard')}}"></span>
                            @else
                            <input class="validate" type="text" name="wireless_standard" value="{{ old('wireless_standard') }}" required>
                            <label for="wireless_standard">Wireless Standard</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">security</i>
                            @if ($errors->has('wireless_security'))
                            <label for="wireless_security">Wireless Security</label>
                            <input class="invalid" type="text" name="wireless_security" value="{{ old('wireless_security') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('wireless_security')}}"></span>
                            @else
                            <input class="validate" type="text" name="wireless_security" value="{{ old('wireless_security') }}" required>
                            <label for="wireless_security">Wireless Security</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">file_download</i>
                            @if ($errors->has('wireless_speed'))
                            <label for="wireless_speed">Wireless Speed</label>
                            <input class="invalid" type="text" name="wireless_speed" value="{{ old('wireless_speed') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('wireless_speed')}}"></span>
                            @else
                            <input class="validate" type="text" name="wireless_speed" value="{{ old('wireless_speed') }}" required>
                            <label for="wireless_speed">Wireless Speed</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">security</i>
                            @if ($errors->has('firewall'))
                            <label for="firewall">Firewalls</label>
                            <input class="invalid" type="text" name="firewall" value="{{ old('firewall') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('firewall')}}"></span>
                            @else
                            <input class="validate" type="text" name="firewall" value="{{ old('firewall') }}" required>
                            <label for="firewall">Firewalls</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">usb</i>
                            @if ($errors->has('usb_sharing'))
                            <label for="usb_sharing">USB Sharing Support</label>
                            <input class="invalid" type="text" name="usb_sharing" value="{{ old('usb_sharing') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('usb_sharing')}}"></span>
                            @else
                            <input class="validate" value="{{ old('usb_sharing') }}" type="text" name="usb_sharing" required>
                            <label for="usb_sharing">USB Sharing Support</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">memory</i>
                            @if ($errors->has('memory'))
                            <label for="memory">Memory</label>
                            <input class="invalid" type="text" name="memory" value="{{ old('memory') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('memory')}}"></span>
                            @else
                            <input class="validate" value="{{ old('memory') }}" type="text" name="memory" required>
                            <label for="memory">Memory</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">network_wifi</i>
                            @if ($errors->has('wireless_frequency'))
                            <label for="wireless_frequency">Wireless Frequencies</label>
                            <input class="invalid" type="text" name="wireless_frequency" value="{{ old('wireless_frequency') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('wireless_frequency')}}"></span>
                            @else
                            <input class="validate" value="{{ old('wireless_frequency') }}" type="text" name="wireless_frequency" required>
                            <label for="wireless_frequency">Wireless Frequencies</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">settings_input_antenna</i>
                            @if ($errors->has('antenna'))
                            <label for="antenna">Antenna</label>
                            <input class="invalid" type="text" name="antenna" value="{{ old('antenna') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('antenna')}}"></span>
                            @else
                            <input class="validate" value="{{ old('antenna') }}" type="text" name="antenna" required>
                            <label for="antenna">Antenna</label>
                            @endif
                        </div>
                        <div class="input-field col s12 m6 l4">
                            <i class="material-icons prefix">date_range</i>
                            @if ($errors->has('release_date'))
                            <label for="release_date">Release Date</label>
                            <input class="invalid datepicker" type="text" name="release_date" value="{{ old('release_date') }}" required>
                            <span class="helper-text" data-error="{{$errors->first('release_date')}}"></span>
                            @else
                            <input class="validate datepicker" value="{{ old('release_date') }}" type="text" name="release_date" required>
                            <label for="release_date">Release Date</label>
                            @endif
                        </div>
                        <div class="input-field col s12">
                            @if ($errors->has('description'))
                            <i class="material-icons prefix">description</i>
                            <label for="description">Description</label>
                            <textarea rows="10" class="invalid materialize-textarea" name="description" required>{{ old('description') }}</textarea>
                            <span class="helper-text" data-error="{{$errors->first('description')}}"></span>
                            @else
                            <i class="material-icons prefix">description</i>
                            <label for="description">Description</label>
                            <textarea rows="10" class="materialize-textarea" name="description" required>{{ old('description') }}</textarea>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-action">
                    <input type="reset" class="btn red" value="Reset">
                    <input type="submit" class="btn blue" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
