<script type="text/javascript">
$(document).ready(function() {
//get data for autocomplete
    $.ajax({
        method: "get",
        url: '{{url("product")}}',
        success: function(response) {
            //convert array to object
            const productArray = response;
            var productData = {};
            for (let i = 0; i < productArray.length; i ++) {
                if (productArray[i].images.length != 0) {
                    const image = window.location.origin + '/' + productArray[i].images[0].path;
                    productData[productArray[i].model] = image;
                }
                else {
                    productData[productArray[i].model] = null;
                }
            }
            $('input.autocomplete').autocomplete({
                data: productData,
            });
        },
        error: function(err) {
            M.toast({html: err.responseJSON.message});
        }
    });
})
</script>