@if($reviews->total() > 0)
<script type="text/javascript">
    var responseRatings = [];
    var ratingsArray = [5, 4, 3, 2, 1];
    $.ajax({
        method: 'get',
        url: `{{url("rating/$product->id")}}`,
        success: function (response) {
            for (let i in ratingsArray) {
                try {
                    if (response[i].rating == ratingsArray[i]) {
                        responseRatings[i] = response[i].count;
                    } else {
                        response.splice(i, 0, null);
                        responseRatings[i] = '0';
                    }
                } catch (e) {
                    responseRatings[i] = '0';
                }
            }
            createStarRatingChart(responseRatings);
        },
        error: function (err) {
            M.toast({
                html: err.responseJSON.message
            });
        }

    })
    const createStarRatingChart = (ratingData) => {
        new Chart($("#review_graph"), {
            type: 'horizontalBar',
            data: {
                labels: ["5", "4", "3", "2", "1", ],
                datasets: [
                    {
                        backgroundColor: ["#3e95cd", "#45b766", "#fff639", "#fd9b39", "#e84c4c "],
                        data: ratingData,
                        categoryPercentage: 1.0,
                        maxBarThickness: 40,
                    },
            ]
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                hover: {
                    mode: null
                },
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            beginAtZero: true,
                            display: false
                        },
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });
    }
</script>
@endif