@if($reviews->total() > 0 || Auth::check())
<script type="text/javascript">
    $(document).ready(function () {
    $('.options').each(function () {
        let user_id = $(this).children("#user_id").val();
        let review_id = $(this).children("#review_id").val();
        
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
        });

        $(this).children("#follow").click(function (e) {
            $.ajax({
                method: "post",
                url: '{{url("follow")}}',
                data: {'user_id': user_id},
                success: function (response) {
                    if (response.valid) {
                        M.toast({html: response.message});
                    }
                    else {
                        M.toast({html: response.message});
                    }
                },
                error: function(err){
                    M.toast({ html: err.responseJSON.message });
                }
            })
        });
        let like_button = $(this).children("#like");
        let dislike_button = $(this).children("#dislike");
        like_button.click(function(e) {
            $.ajax({
                method: 'post',
                url: `/review/like`,
                data: {'review_id': review_id},
                success: function(response) {
                    like_button.html(`<i class='material-icons left'>thumb_up</i>` + response.likes);
                    dislike_button.html(`<i class='material-icons left'>thumb_down</i>` + response.dislikes);
                },
                error: function(err) {
                    M.toast({ html: err.responseJSON.message });
                }
            });
        });
        dislike_button.click(function(e) {
            $.ajax({
                method: 'post',
                url: `/review/dislike`,
                data: {'review_id': review_id},
                success: function(response) {
                    like_button.html("<i class='material-icons left'>thumb_up</i>" + response.likes);
                    dislike_button.html("<i class='material-icons left'>thumb_down</i>" + response.dislikes);
                },
                error: function(err) {
                    M.toast({ html: err.responseJSON.message });
                }
            });
        });
    });
});
</script>
@endif