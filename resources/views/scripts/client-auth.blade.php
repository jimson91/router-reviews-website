<script type="text/javascript">
$(document).ready(function () {
    $('.trigger-modal').click(function () {
        const data = $(this).data();
        $(data.modal).fadeToggle();
        $(data.target).fadeToggle();
        $('body').css('overflow', 'hidden');
        $('.card').css('z-index', '-1');
        $('.custom-card').css('z-index', '2');
    });
    $('.modal-exit').click(function () {
        const data = $(this).data();
        $(data.modal).fadeToggle();
        $(data.target).fadeToggle();
        $('.card').css('z-index', '1');
        $('body').css('overflow', 'auto');
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#login-form').submit(function (event) {
        event.preventDefault();
        var email = $('#login-email').val();
        var password = $('#login-password').val();
        $.ajax({
            method: "post",
            url: '{{url("login")}}',
            data: {
                'email': email,
                'password': password
            },
            success: function (response) {
                if ($.isEmptyObject(response.error)) {
                    window.location.reload();
                } else {
                    $('#login-form input[class="invalid"]').attr('class', 'validate');
                    for (let i in response.error) {
                        $(`#login-form #helper-${i}`).attr('data-error', response.error[i][0]);
                        $(`#login-form input[name=${i}]`).attr('class', 'invalid');
                    }
                }
            },
            error: function (err) {
                M.toast({
                    html: err.responseJSON.message
                });
            }
        });
    });
    $('#register-form').submit(function (event) {
        event.preventDefault();
        var form = {
            'first_name': $('#first_name').val(),
            'last_name': $('#last_name').val(),
            'date_birth': $('#date_birth').val(),
            'email': $('#register_email').val(),
            'password': $('#register_password').val(),
            'password_confirmation': $('#register_password_confirm').val()
        }
        $.ajax({
            method: 'post',
            url: '{{url("register")}}',
            data: form,
            success: function (response) {
                if ($.isEmptyObject(response.error)) {
                    window.location.href = window.location.origin + '/dashboard';
                } else {
                    $('#register-form input[class="invalid"]').attr('class', 'validate');
                    for (let i in response.error) {
                        $(`#register-form #helper-${i}`).attr('data-error', response.error[i][0]);
                        $(`#register-form input[name=${i}]`).attr('class', 'invalid');
                    }
                }
            },
            error: function (err) {
                M.toast({
                    html: err.responseJSON.message
                });
            }
        })
    })
});
</script>