<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="James H">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
    <link rel="shortcut icon" type="image/ico" href="{{asset('favicon.png')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="{{asset('js/jquery.matchHeight.js')}}" type="text/javascript"></script>
</head>
<!--No JavaScript Alert -->
<noscript>
    <style type="text/css">
        nav,
        .card,
        .valign-wrapper {
            display: none;
        }
    </style>
    <div class="card-panel red lighten-1 white-text center">
        <i class="material-icons">warning</i>
        <p>This page needs JavaScript enabled to work properly</p>
    </div>
</noscript>

<body>
    <nav>
        <div class="nav-wrapper deep-purple darken-3">
            <a href="#" data-target="mobile-demo" class="sidenav-trigger">
                <i class="material-icons">menu</i>
            </a>
            <a href="#" class="brand-logo center">
                <i class="material-icons">router</i>
                Router Reviews
            </a>
            <ul class="left hide-on-med-and-down">
                <li class="{{ Request::segment(1) === 'product' ? 'active' : null }}">
                    <a href="/product">
                        <i class="material-icons left">search</i>
                        Search Router
                    </a>
                </li>
                <li class="{{ Request::segment(1) === 'vendor' ? 'active' : null }}">
                    <a href="/vendor">
                        <i class="material-icons left">search</i>
                        Search Vendor
                    </a>
                </li>
            </ul>
            <ul class="sidenav" id="mobile-demo">
                <li class="{{ Request::segment(1) === 'product' ? 'active' : null }}">
                    <a href="/product">
                        <i class="material-icons left">search</i>
                        Search Router
                    </a>
                </li>
                <li class="{{ Request::segment(1) === 'vendor' ? 'active' : null }}">
                    <a href="/vendor">
                        <i class="material-icons left">search</i>
                        Search Vendor
                    </a>
                </li>
                @guest
                <li class="{{ Request::segment(1) === 'login' ? 'active' : null }}">
                    <a class="trigger-modal" data-target="#login-modal" data-modal="#login-backdrop">
                        <i class="material-icons left">account_circle</i>
                        Login
                    </a>
                </li>
                <li class="{{ Request::segment(1) === 'register' ? 'active' : null }}">
                    <a class="trigger-modal" data-target="#register-modal" data-modal="#register-backdrop">
                        <i class="material-icons left">fiber_new</i>
                        Register
                    </a>
                </li>
                @else
                <li>
                    <a href="/dashboard">
                        <i class="material-icons left">dashboard</i>
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="material-icons left">exit_to_app</i>
                        Logout
                    </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                @endguest
            </ul>
            @guest
            <ul class="right hide-on-med-and-down">
                <li>
                    <a class="trigger-modal" data-target="#login-modal" data-modal="#login-backdrop">
                        <i class="material-icons left">account_circle</i>
                        Login
                    </a>
                </li>
                <li>
                    <a class="trigger-modal" data-target="#register-modal" data-modal="#register-backdrop">
                        <i class="material-icons left">fiber_new</i>
                        Register
                    </a>
                </li>
            </ul>
            @else
            <ul class="right hide-on-med-and-down">
                <li class="{{ Request::segment(1) === 'dashboard' ? 'active' : null }}">
                    <a href="/dashboard">
                        <i class="material-icons left">dashboard</i>
                        Dashboard</a>
                </li>
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="material-icons left">exit_to_app</i>
                        Logout
                    </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
            @endguest
        </div>
    </nav>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.match-height').matchHeight();
            $('.sidenav').sidenav();
            $('.tooltipped').tooltip();
            $('.datepicker').datepicker();
            $('.collapsible').collapsible();
            $('.modal').modal();
            $('select').formSelect();
            $('.slider').slider();
            $('.materialboxed').materialbox();
            $('.dropdown-trigger').dropdown();
            $('.tabs').tabs({
                swipeable: false
                , responsiveThreshold: 1920
            });
        });

    </script>
    @guest
    @include('modals.login')
    @include('modals.register')
    @include('scripts.client-auth')
    @endguest
    @yield('content')
</body>

</html>
