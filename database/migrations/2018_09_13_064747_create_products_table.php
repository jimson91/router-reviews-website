<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->date('release_date')->date("d-m-y");
            $table->integer('vendor_id');
            $table->string('wireless_frequency');
            $table->string('antenna');
            $table->string('wireless_standard');
            $table->string('wireless_security');
            $table->string('wireless_speed');
            $table->string('firewall');
            $table->string('memory')->nullable();
            $table->string('usb_sharing');
            $table->text('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
