<?php

use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            'product_id' => '1',
            'user_id' => '3',
            'path' => 'storage/product_images/ASUS.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '2',
            'user_id' => '3',
            'path' => 'storage/product_images/rapture_GT-AC5300.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '3',
            'user_id' => '3',
            'path' => 'storage/product_images/RT-AC68U_AC1900.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '4',
            'user_id' => '3',
            'path' => 'storage/product_images/RT-AC58U_AC1300.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '5',
            'user_id' => '3',
            'path' => 'storage/product_images/CVR100W.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '6',
            'user_id' => '2',
            'path' => 'storage/product_images/DIR-890L-1.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '6',
            'user_id' => '1',
            'path' => 'storage/product_images/DIR-890L-2.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '7',
            'user_id' => '1',
            'path' => 'storage/product_images/DSL-3785_AC1200.png',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '8',
            'user_id' => '1',
            'path' => 'storage/product_images/Archer_C7_AC1750.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '9',
            'user_id' => '1',
            'path' => 'storage/product_images/Archer_AC2300.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '10',
            'user_id' => '1',
            'path' => 'storage/product_images/TD-W8960N.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '11',
            'user_id' => '1',
            'path' => 'storage/product_images/R7000P_Nighthawk_AC2300.webp',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '12',
            'user_id' => '1',
            'path' => 'storage/product_images/Nighthawk_X6_R8000_AC3200.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '13',
            'user_id' => '1',
            'path' => 'storage/product_images/WRT1200AC_AC1200.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '14',
            'user_id' => '1',
            'path' => 'storage/product_images/WRT32X_AC3200.jpeg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('images')->insert([
            'product_id' => '15',
            'user_id' => '1',
            'path' => 'storage/product_images/EA9200_AC3200.jpg',
            'updated_at' => \DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
