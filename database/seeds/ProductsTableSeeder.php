<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'model' => 'BRT-AC828 AC2600',
            'release_date' => "31/05/2016",
            'vendor_id' => '1',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 4',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => '64-bit WEP, 128-bit WEP, WPA2-PSK, WPA-PSK, WPA-Enterprise , WPA2-Enterprise , WPS support, Radius with 802.1x',
            'wireless_speed' => 'Up to 1734 Mbps',
            'firewall' => 'SPI, DoS Protection, Intrusion Detection',
            'memory' => '256 MB Flash, 512 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "Dual-band AC2600 Wi-Fi not only allows fast and stable connections to multiple devices, but it also offers superb coverage — up to 100 meters on the 2.4GHz band in open areas* — that's perfect for the typical office. Even in complex office layouts, the powerful 4x4 (4-transmit, 4-receive) MU-MIMO design with four external antennas and fine-tuned power output ensures optimized reception for the best possible performance.",
        ]);
            
        DB::table('products')->insert([
            'model' => 'Rapture GT-AC5300',
            'release_date' => "31/05/2016",
            'vendor_id' => '1',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 8',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => 'Up to 2167 Mbps',
            'firewall' => 'SPI, DoS Protection, Intrusion Detection',
            'memory' => '256 MB Flash, 1024 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "Designed with the gamer in mind, the ROG Rapture GT-AC5300 Wireless Tri-Band Gigabit Router from ASUS features a variety of optimizations to enhance network connectivity, increase stability, and boost overall network performance. Take advantage of optimized gaming ports which prioritize gaming traffic while the Gamers Private Network allows gamers to connect to private game servers to further enhance network performance. If that's not enough, the ROG Rapture GT-AC5300 features enhanced network security complete with its own IPS (Intrusion Prevention System) backed by Trend Micro technology.",
        ]);
            
        DB::table('products')->insert([
            'model' => 'RT-AC68U AC1900',
            'release_date' => "09/10/2013",
            'vendor_id' => '1',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 3',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => '64-bit WEP, 128-bit WEP, WPA2-PSK, WPA-PSK, WPA-Enterprise , WPA2-Enterprise , WPS support',
            'wireless_speed' => 'Up to 1300 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "AC1900 Dual band whole home mesh wifi system for large and multi-story homes, AiProtection network security powered by Trend Micro, supports flexible SSID setting, wired inter-router connections, and Adaptive QoS.",
        ]);
            
        DB::table('products')->insert([
            'model' => 'RT-AC58U AC1300',
            'release_date' => "28/08/2016",
            'vendor_id' => '1',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 4',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => '64-bit WEP, 128-bit WEP, WPA2-PSK, WPA-PSK, WPA-Enterprise , WPS support',
            'wireless_speed' => 'Up to 867 Mbps',
            'firewall' => 'None',
            'memory' => '128 MB Flash, 128 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "The ASUS RT-AC58U AC1300 Dual-band Gigabit Wi-Fi Router with four high-gain antennas and high power technology delivers extremely long operating range. Its 802.11ac high-speed performance is perfect for streaming up to 4K Ultra HD videos, online gaming with support for latest gen consoles such as Xbox One and PlayStation 4 and performing other bandwidth-intensive tasks. Multi-purpose USB 3.0 port allow printer, hard drive and 3G/4G sharing; while ASUS AiCloud support allows you to access, share, and stream files from your home PC to internet-connected devices.",
        ]);
            
        DB::table('products')->insert([
            'model' => 'CVR100W',
            'release_date' => "31/05/2016",
            'vendor_id' => '2',
            'wireless_frequency' => '2.4GHz',
            'antenna'=> 'Built-in antenna x 2',
            'wireless_standard' => 'IEEE 802.11 b/g/n',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => 'Up to 100 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '256 MB Flash',
            'usb_sharing' => 'No',
            'description' => "Desktop · NAT support · auto-uplink (auto MDI/MDI-X) · MAC address filtering · Low-latency queuing (LLQ) · Class-Based Weighted Fair Queuing (CBWFQ) · Weighted Fair Queuing (WFQ) · Spanning Tree Protocol (STP) support · Access Control List (ACL) support · Quality of Service (QoS).",
        ]);
            
        DB::table('products')->insert([
            'model' => 'DIR-890L AC3200',
            'release_date' => "31/05/2016",
            'vendor_id' => '3',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => 'Up to 1300Mbps Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "The D-Link DIR-890L is a wired &wireless router that enables you to create your own network with its 1300 Mbps wireless speed. Supporting the wireless AC connectivity and wired throughput via 4-port Gigabit LAN interface, this device allows seamless functioning of applications that use multiple bandwidths.",
        ]);
            
        DB::table('products')->insert([
            'model' => 'DSL-3785 AC1200',
            'release_date' => "31/05/2016",
            'vendor_id' => '3',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "The D-Link MU-MIMO Modem Router is a great option for when you want to share your high-speed DSL or NBN internet connection with both wired and wireless devices. It utilises dual-band AC1200 WiFi that can be used to connect desktop computers, gaming consoles, smart TVs, phones and more and 2 USB ports for file sharing.",
        ]);
            
        DB::table('products')->insert([
            'model' => 'Archer C7 AC1750',
            'release_date' => "31/05/2016",
            'vendor_id' => '4',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'Stateful Packet Inspection (SPI), Deny of Service (DoS) Prevention',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "The Next Generation of Wi-Fi​TP-LINK’s Archer C7 comes with the next generation Wi-Fi standard – 802.11ac, 3 times faster than wireless N speeds and delivering a combined wireless data transfer rate of up to 1.75Gbps. With 1300Mbps wireless speeds over the crystal clear 5GHz band and 450Mbps over the 2.4GHz band, the Archer C7 is the superior choice for seamless HD streaming, online gaming and other bandwidth-intensive tasks.",
        ]);
            
        DB::table('products')->insert([
            'model' => 'Archer AC2300',
            'release_date' => "31/05/2016",
            'vendor_id' => '4',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "The Archer C3200 wireless router uses Tri-Band technology to run three separate Wi-Fi channels at once - creating a network that can connect to more devices without a trade-off in performance.",
        ]);

        DB::table('products')->insert([
            'model' => 'TD-W8960N',
            'release_date' => "31/05/2016",
            'vendor_id' => '4',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "TP-Link Wireless N ADSL2+ Modem Router with 4 ports boasts support of up to 10 IPsec VPN tunnels simultaneously to allow for maximum flexibility when accessing files, peripherals, media or even other computers. The W9970 is the ultimate choice for seamless high definition video streaming, online gaming and other bandwidth intensive tasks.",
        ]);

        DB::table('products')->insert([
            'model' => 'R7000P Nighthawk AC2300',
            'release_date' => "31/05/2016",
            'vendor_id' => '5',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "The NETGEAR Nighthawk AC2300 Smart WiFi Router provides up to 1625 + 600 Mbps WiFi and MU-MIMO for lag-free gaming, better video streaming and multiple mobile devices. With Gigabit WiFi, Beamforming, and built-in high-powered amplifiers and external antennas, get ready for extreme performance! Manage it from anywhere with NETGEAR genie remote access.",
        ]);

        DB::table('products')->insert([
            'model' => 'Nighthawk X6 R8000 AC3200',
            'release_date' => "31/05/2016",
            'vendor_id' => '5',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "The Nighthawk X6 R8000 Router has high performance Tri-Band WiFi technology that enhances gaming, streaming and mobile use and allows wireless speeds of up to 3.2Gbps. The X6 cleverly assigns each device to its optimal WiFi band so that it can reach its maximum speed.",
        ]);

        DB::table('products')->insert([
            'model' => 'WRT1200AC AC1200',
            'release_date' => "31/05/2016",
            'vendor_id' => '6',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "Built on the foundation of our original WRT's open-source heritage, the Linksys WRT1200AC delivers superior network performance for the most demanding users. The WRT1200AC features two external antennas, a powerful 1.3GHz dual-core ARM, and Wireless-AC to provide high-speed Wi-Fi connections with exceptional range. For competitive performance, we equipped the WRT1200AC with four Gigabit Ethernet ports featuring speeds up to 10 times faster than Fast Ethernet.",
        ]);

        DB::table('products')->insert([
            'model' => 'WRT32X AC3200',
            'release_date' => "23/03/2018",
            'vendor_id' => '6',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "Engineered to cater to the demands of gamers, the Linksys WRT32X AC3200 WiFi Gaming Router features a powerful dual-core CPU for high-speed data processing and the Killer Prioritization Engine for optimised network traffic. Equipped with 4 high-performance antennas, it helps ensure superior Wi-Fi signal strength regardless of where it’s placed inside your home or office.",
        ]);

        DB::table('products')->insert([
            'model' => 'EA9200 AC3200',
            'release_date' => "31/05/2016",
            'vendor_id' => '6',
            'wireless_frequency' => '2.4GHz, 5GHz',
            'antenna'=> 'External antenna x 6',
            'wireless_standard' => 'IEEE 802.11 a/b/g/n/ac',
            'wireless_security' => 'WPA-PSK/WPA2-PSK, WPS',
            'wireless_speed' => '867 Mbps',
            'firewall' => 'SPI, DoS Protection',
            'memory' => '128 MB Flash, 256 MB RAM',
            'usb_sharing' => 'Yes',
            'description' => "Designed especially for today's mobile-device household - the ac3200 tri-band smart wi-fi router offers exceptionally fast combined speeds of up to 3.2 gbps - up to double the speeds of a dual-band router.",
        ]);
    }
}
