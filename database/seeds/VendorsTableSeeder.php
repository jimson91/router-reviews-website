<?php

use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendors')->insert([
            'name' => 'Asus',
            'logo' => "storage/vendors/asus.png"
        ]);
        
        DB::table('vendors')->insert([
            'name' => 'Cisco Systems',
            'logo' => "storage/vendors/cisco.png"
        ]);
        
        DB::table('vendors')->insert([
            'name' => 'D-Link',
            'logo' => "storage/vendors/d-link.png"
        ]);
        
        DB::table('vendors')->insert([
            'name' => 'TP-Link',
            'logo' => "storage/vendors/tp-link.png"
        ]);

        DB::table('vendors')->insert([
            'name' => 'Netgear',
            'logo' => "storage/vendors/netgear.png"
        ]);

        DB::table('vendors')->insert([
            'name' => 'Linksys',
            'logo' => "storage/vendors/linksys.png"
        ]);

        DB::table('vendors')->insert([
            'name' => 'Google',
            'logo' => "storage/vendors/google.png"
        ]);

        DB::table('vendors')->insert([
            'name' => 'Huawei',
            'logo' => "storage/vendors/huawei.png"
        ]);

        DB::table('vendors')->insert([
            'name' => 'Apple',
            'logo' => "storage/vendors/apple.png"
        ]);
    }
}
