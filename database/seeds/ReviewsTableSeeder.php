<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '3',
            'product_id' => '1',
            'user_id' => '1',
            'text' => "Pretty poor range (actually get better range if I just connect direct to my router which kind of defeats the purpose of buying this). Slow when used as a repeater (my previous Netgear EX6200 AC1300 was faster), When accessing over VPN to home router unable to reach shared USB connected storage using Samba on repeater (ftp works fine though), Needs rebooting occasionally which is inconvenient, Feel let down with this product considering its high price, was expecting better performance for this price tag.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '2',
            'product_id' => '6',
            'user_id' => '5',
            'text' => "Bought to replace a dual band Netgear on its last legs. Set it up and reception on the same floor was great across all 3 bands. Floors/rooms above were good too. Lower floor (not a basement-have 4 story town-home) was horrific reception.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '4',
            'product_id' => '6',
            'user_id' => '4',
            'text' => "This router is very powerful, has excellent range & looks really nice! Some things I have noticed: More advanced menus & options & logs showing up on the menu screen are unavailable like on older D-Link routers, such as the DIR-655. If you have a wireless bridge, any devices on that bridge will show up every so often in the same spot on the clients page, instead of independently, and it will not update the amount of clients. ",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '5',
            'product_id' => '6',
            'user_id' => '3',
            'text' => "Incredibly fast router, almost lightyears ahead of others as reviews from CNET, PCMag and Router Authority suggested. Would highly highly recommend, as it was a breeze to install. The biggest thing to note is that this router is not small and goes better on a shelf than a high point on a wall due to placement / length of power and ethernet connections.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '5',
            'product_id' => '6',
            'user_id' => '2',
            'text' => "This is unquestionably the best router I've owned as far as reliability and performance. I upgraded from a DIR-868L. The range is better than my 868L but not quite what I was hoping for given the 6 antenna configuration. I still have to use an extender for the upstairs to get full speed. I have a fairly big house and have the 890L located downstairs on the left side of the house. My ISP provides a 85 MB/sec. connection and my devices upstairs on the right side of the house can only sustain about a 25MB/sec.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '1',
            'product_id' => '6',
            'user_id' => '1',
            'text' => "I definitely do not recommend this router. Not consistent. I'd prefer a budget device with consistently slow throughput than inconsistent quick bursts of speed with service loss. In fact, due to this experience and this router, I will never suggest a D-LINK product again.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '5',
            'product_id' => '7',
            'user_id' => '2',
            'text' => "Pros: Excellent performance, and is actually cool to the touch! Cons: Setting up VDSL (NBN FTTN) required a firmware update and a setting which was named differently in my Aussie Broadband setup information. I believe it was called Dynamic IP DHCP instead of PPPoA by the setup information. Smooth sailing after that.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '4',
            'product_id' => '7',
            'user_id' => '1',
            'text' => "Bought to replace my optus branded sagecom modem router for fttn nbn, and this new modem/router is infinitely better. Does what it says it will do.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '5',
            'product_id' => '8',
            'user_id' => '1',
            'text' => "I'm very happy with my TP-LINK TL-WDR4300 N750 router, which is practically identical in looks and features, at least on the outside, to the Archer C7 AC1750. The main differences you notice on the surface, are the blue lights (N750) vs green lights (AC1750), and the larger power brick on the AC1750.",
        ]);
        
        DB::table('reviews')->insert([
            'created_at' => "27-08-2018",
            'rating' => '2',
            'product_id' => '2',
            'user_id' => '1',
            'text' => "I bought this as an upgrade to an ASUS 68U. I have 4 other 68U as Aimesh nodes in my network and noticed the nodes transiently dropping throughout the day and night. The CPU on the router ranged from 60-100% all the time even after a hard reset and I figured it was time for an upgrade.",
        ]);
    }
}
