<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => "Bob",
            'last_name' => 'Barker',
            'date_birth' => "02/11/1956",
            'email' => "bob@example.com",
            'password' => bcrypt('123456'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'first_name' => "John",
            'last_name' => "Smith",
            'date_birth' => "25/06/1985",
            'email' => "john@example.com",
            'password' => bcrypt('123456'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'first_name' => "Fred",
            'last_name' => "Jackson",
            'date_birth' => "18/02/1992",
            'email' => "fred@example.com",
            'password' => bcrypt('123456'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'first_name' => "James",
            'last_name' => "Mackoy",
            'date_birth' => "12/04/1995",
            'email' => "moderator@example.com",
            'password' => bcrypt('123456'),
            'moderator' => 1,
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'first_name' => "Bill",
            'last_name' => "Burr",
            'date_birth' => "09/07/1967",
            'email' => "bill@example.com",
            'password' => bcrypt('123456'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
        DB::table('users')->insert([
            'first_name' => "Debra",
            'last_name' => "Wilson",
            'date_birth' => "23/03/1974",
            'email' => "debra@example.com",
            'password' => bcrypt('123456'),
            'created_at' => \DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
