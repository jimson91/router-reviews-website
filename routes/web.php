<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return view('index');
});
/*=================== Product Routes =============================*/
route::resource('product', 'ProductController');
Route::get('search', 'ProductController@search')->name('product.search');
Route::get('rating/{product_id}', 'ProductController@rating')->name('product.rating');

/*=================== Vendor Routes ========================*/
Route::get('vendor', 'VendorController@index')->name('vendor.index');
Route::get('vendor/{name}', 'VendorController@show')->name('vendor.show');

/*=================== Review Routes ===============================*/
Route::resource('review', 'ReviewController', [
    'except' => ['index', 'create', 'show']
]);

Route::post('review/like', 'ReviewController@like')->name('like');
Route::post('review/dislike', 'ReviewController@dislike')->name('dislike');

/*=================== Dashboard & Authentication Routes ===============================*/
Auth::routes();
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::post('/follow', 'DashboardController@addFollow')->name('follow');
Route::get('/unfollow/{user_id}', 'DashboardController@removeFollow')->name('unfollow');
Route::post('/avatar', 'DashboardController@avatar')->name('dashboard.avatar');

Route::post('image', 'ImageController@store')->name('image.store');