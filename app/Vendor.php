<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Vendor extends Model
{
    use Sortable;

    public $timestamps = false;
    
    protected $sortable = ['name'];

    protected $sortableAs = ['products_count'];
    
    function products() {
        return $this->hasMany('App\Product');
    }
}
