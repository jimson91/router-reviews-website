<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Review;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_reviews = Auth::user()->reviews()->get();
        $followers = Auth::user()->followers;
        $feed = array();

        foreach($followers as $follower) {
            $getReviews = Review::where('user_id', $follower->id)->get();
            $feed[] = $getReviews;
        }

        return view ('dashboard')->with('user_reviews', $user_reviews)->with('followers', $followers)->with('feed', $feed);
        /*if ($user->moderator == 0) {
            
            return view('user_dashboard')->with('user_reviews', $user_reviews)->with('followers', $followers)->with('feed', $feed);
        } 
        else {
            return view ('moderator_dashboard')->with('user_reviews', $user_reviews)->with('followers', $followers)->with('feed', $feed);
        }*/
    }

    public function avatar(Request $request) {
        $this->validate($request, [ 
            'image' => 'required',
        ]);
        
        $request->file('image')->store('avatars','public');
        $path = 'storage/avatars/'.$request->image->hashName();
        $user = Auth::user();
        if ($user->avatar !== "storage/avatars/default.jpg") {
            unlink($user->avatar);
        }
        $user->avatar = $path;
        $user->save();

        return redirect("/dashboard");
    }

    public function addFollow(Request $request)
    {
        $id = $request->user_id;
        $user = User::find($id);
        $username = $user->getFullName();

        $already_added = false;

        foreach (Auth::user()->followers as $follower) {
            if ($follower->id == $id) {
                $already_added = true;
                break;
            }
        }
        if ($already_added) {
            return response()->json(['valid'=>false, 'message'=>'You are already following '.$username]);
        }
        else {
            
        Auth::user()->follow($user);
        return response()->json(['valid'=>true, 'message'=>'You are now following '.$username]);
        }
    }

    public function removeFollow($id)
    {
        $user = User::find($id);
        Auth::user()->unfollow($user);
        return Redirect()->back();
    }
}
