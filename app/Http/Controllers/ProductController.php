<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Vendor;

class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show', 'search', 'rating']]);
        $this->middleware('moderator', ['except' => ['index', 'show', 'search', 'rating']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax() || $request->isJson()) {
            $products = Product::select('id', 'model', 'vendor_id', 'release_date')
            ->with('images')
            ->get();
            return response()->json($products);
        }
        else {
            $products = Product::select('id', 'model', 'vendor_id', 'release_date', 'description')
            ->withCount(['vendor','reviews'])
            ->with('images')
            ->sortable()
            ->paginate(10);

            return view('products.index')->with('products', $products);
        }
    }

    public function search(Request $request)
    {
        $searchterm = $request->term;

        $products = Product::withCount(['vendor','reviews'])
        ->where('model','LIKE','%'.$searchterm."%")
        ->sortable()
        ->paginate(10);

        return view('products.index')
        ->with('products', $products)
        ->with('searchterm', $searchterm);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
    
        return view('products.create_form')->with('vendors', Vendor::all());
    
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 
            'model' => 'required|unique:products|min:5|max:200',
            'release_date' => 'required|date',
            'vendor' => 'exists:vendors,id',
            'wireless_frequency' => 'required|min:6',
            'antenna' => 'required|min:1',
            'wireless_standard' => 'required|min:6',
            'wireless_security'=> 'required|min:6,',
            'wireless_speed' => 'required:|min:6',
            'firewall' => 'required|min:6',
            'memory' => 'required|min:6',
            'usb_sharing' => 'required|min:2',
            'description' => 'required|min:20|max:200'
        ]);
        
        $product = new Product();
        $product->model = $request->model;
        $product->release_date = $request->release_date;
        $product->vendor_id = $request->vendor;
        $product->wireless_frequency = $request->wireless_frequency;
        $product->antenna = $request->antenna;
        $product->wireless_standard = $request->wireless_standard;
        $product->wireless_security = $request->wireless_security;
        $product->wireless_speed = $request->wireless_speed;
        $product->firewall = $request->firewall;
        $product->memory = $request->memory;
        $product->usb_sharing = $request->usb_sharing;
        $product->description = $request->description;
        $product->save();
        return redirect("/product/$product->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function show($id)
    {
        $product = Product::find($id);

        $vendor = $product->vendor()->first();
        
        $reviews = $product->reviews()
        ->with('likes','dislikes')
        ->with('user')
        ->sortable()
        ->orderBy('created_at','desc')
        ->paginate(4);

        $images = $product->images()
        ->with('user')
        ->get();
        
        return view('products.show')
        ->with('product', $product)
        ->with('vendor', $vendor)
        ->with('reviews', $reviews)
        ->with('images', $images);
    }

    public function rating($id) {
        $product = Product::find($id);
        
        $ratings = $product->reviews()
        ->selectRaw('rating, count(*) as count')
        ->groupBy('rating')
        ->orderBy('rating','desc')
        ->get();
 
        return response()->json($ratings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('products.edit_form')
        ->with('product', $product)
        ->with('vendors', Vendor::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [ 
            'model' => 'required|min:5|max:200',
            'release_date' => 'required|date',
            'vendor' => 'exists:vendors,id',
            'wireless_frequency' => 'required|min:6',
            'antenna' => 'required|min:1',
            'wireless_standard' => 'required|min:6',
            'wireless_security'=> 'required|min:6,',
            'wireless_speed' => 'required:|min:6',
            'firewall' => 'required|min:6',
            'memory' => 'required|min:6',
            'usb_sharing' => 'required|min:2',
            'description' => 'required|min:20|max:200'
        ]);
        
        $product = Product::find($id);
        $product->model = $request->model;
        $product->release_date = $request->release_date;
        $product->vendor_id = $request->vendor;
        $product->wireless_frequency = $request->wireless_frequency;
        $product->antenna = $request->antenna;
        $product->wireless_standard = $request->wireless_standard;
        $product->wireless_security = $request->wireless_security;
        $product->wireless_speed = $request->wireless_speed;
        $product->firewall = $request->firewall;
        $product->memory = $request->memory;
        $product->usb_sharing = $request->usb_sharing;
        $product->description = $request->description;
        $product->save();
        return redirect ("/product/$product->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $product = Product::find($id);
        $images = $product->images()->delete();
        $reviews = $product->reviews()->with('likes')->delete();
        $product->delete();
        
        return redirect("/product");
    }
}
