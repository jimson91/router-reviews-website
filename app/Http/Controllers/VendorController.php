<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\Product;

class VendorController extends Controller
{
    public function index()
    {
        $vendors = Vendor::withCount('products')
        ->sortable()
        ->orderBy('name', 'asc')
        ->paginate(10);
        
        return view('vendors.index')->with('vendors', $vendors);
    }

    public function show($id) 
    {
        $products = Product::where('vendor_id', '=', $id)
        ->with('vendor')
        ->withCount('reviews')
        ->sortable()
        ->paginate(10);
        
        $vendor = Vendor::where('id', '=', $id)->first();

        return view('products.index')
        ->with('vendor', $vendor)
        ->with('products', $products);
    }
}
