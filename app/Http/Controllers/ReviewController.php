<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Review;
use App\Product;
use App\Like;
use App\Dislike;

class ReviewController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        /*Unable to find an exception to allow current user to edit/delete THEIR review
        Custom checks are implemented on line 119 & 160
        $this->middleware('moderator', ['only' => 'edit']);
        $this->middleware('moderator', ['only' => 'destroy']);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 
            'rating' => 'required',
            'text' => 'required|min:20',
            'product_id' => 'exists:products,id',
            'user_id' => 'exists:users,id'
        ]);

        $isUnique = Product::find($request->product_id)
        ->reviews()
        ->where('user_id', $request->user_id)
        ->count();

        if($isUnique == 0) {
        $review = new Review();
        $review->rating = $request->rating;
        $review->text = $request->text;
        $review->product_id = $request->product_id;
        $review->user_id = $request->user_id;
        $review->save();
        return redirect("/product/$request->product_id");
        }
        else {
            return redirect("/product/$request->product_id")->withErrors([
                'duplicate' => ['You have have already submitted a review for this model']
            ]);
        }
    }

    public function like(Request $request) {

        $review_id = $request->review_id;
        $review = Review::find($review_id);
        $user = Auth::user();

        $like = $user->likes()
        ->where('review_id', $review_id)
        ->first();
        
        $dislike = $user->dislikes()
        ->where('review_id', $review_id)
        ->first();

        if($dislike) {
            $dislike->delete();
        }
        if($like) {
            $like->delete();
        }
        else {
            $like = new Like();
            $like->user_id = $user->id;
            $like->review_id = $review_id;
            $like->save();
        }

        $likes_count = count($review->likes()
        ->where('review_id', $review_id)
        ->get());

        $dislikes_count = count($review->dislikes()
        ->where('review_id', $review_id)
        ->get());
        
        return response()->json(['likes'=>$likes_count, 'dislikes'=>$dislikes_count]);
    }
    
    public function dislike(Request $request) {
        
        $review_id = $request->review_id;
        $review = Review::find($review_id);
        $user = Auth::user();

        $like = $user->likes()
        ->where('review_id', $review_id)
        ->first();
        
        $dislike = $user->dislikes()
        ->where('review_id', $review_id)
        ->first();

        if ($like) {
            $like->delete();
        }
        if ($dislike) {
            $dislike->delete();
        } 
        else {
            $dislike = new Dislike();
            $dislike->user_id = $user->id;
            $dislike->review_id = $review_id;
            $dislike->save();
        }

        $likes_count = count($review->likes()
        ->where('review_id', $review_id)
        ->get());
        
        $dislikes_count = count($review->dislikes()
        ->where('review_id', $review_id)
        ->get());
        
        return response()->json(['likes'=>$likes_count, 'dislikes'=>$dislikes_count]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::find($id);
        $user = Auth::user();
        //Check if the review is associated to the user, or is a moderator
        if ($user->id == $review->user_id || $user->moderator == 1) {
            return view('reviews.edit_form')->with('review', $review);
        } else {
            return redirect("/product/$review->product_id");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [ 
            'rating' => 'required',
            'text' => 'required|min:20',
            'product_id' => 'exists:products,id'
        ]);
        
        $review = Review::find($id);
        $review->rating = $request->rating;
        $review->text = $request->text;
        $review->save();
        return redirect ("/product/$review->product_id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $review = Review::find($id);
        $user = Auth::user();

        if ($user->id == $review->user_id || $user->moderator == 1) {

            $likes = $review->likes()->delete();
            $dislikes = $review->dislikes()->delete();
            $review->delete();
            return redirect("/product/$review->product_id");
        }
        else {
            return redirect("/product/$review->product_id");
        }
    }
}
