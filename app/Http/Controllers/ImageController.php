<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Product;

class ImageController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function store(Request $request) {

        $this->validate($request, [ 
            'product_id' => 'exists:products,id',
            'user_id' => 'exists:users,id',
            'image' => 'required',
        ]);
        
        $isUnique = true;

        $product_images = Product::find($request->product_id)
        ->images()
        ->get();

        foreach($product_images as $product_image) {
            if ($product_image->user_id == $request->user_id) {
                $isUnique = false;
            }
        }

        if($isUnique) {
        $request->file('image')->store('product_images','public');
        /*First we must set the initial path to the directory where images are stored, then we append the file name on the end
          by calling either "hashName()" or "getClientOriginalName()" More Info Here: https://laracasts.com/discuss/channels/laravel/how-to-get-uploaded-file-name-in-laravel-53*/
        $path = 'storage/product_images/'.$request->image->hashName();
            
        $image = new Image();
        $image->path = $path;
        $image->product_id = $request->product_id;
        $image->user_id = $request->user_id;
        $image->save();
        return redirect("/product/$request->product_id");
        } 
        
        else {
        
            return redirect("/product/$request->product_id");
        }
    }
}
