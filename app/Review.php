<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Review extends Model
{
    use Sortable;

    public $timestamps = false;
    
    protected $sortable = ['rating', 'created_at'];

    function product() {
        return $this->belongsTo('App\Product');
    }
    
    function user() {
        return $this->belongsTo('App\User');
    }

    function likes() {
        return $this->hasMany('App\Like');
    }
    
    function dislikes() {
        return $this->hasMany('App\Dislike');
    }
}
