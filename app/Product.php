<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    use Sortable;

    protected $fillable = ['model', 'details'];
    
    public $timestamps = false;
    
    protected $sortable = ['model', 'vendor_id', 'release_date'];
    
    protected $sortableAs = ['reviews_count'];

    function vendor() {
        return $this->belongsTo('App\Vendor');
    }

    function avgRating() {
        return round($this->reviews()->avg('rating'), 1);
    }
    
    function reviews() {
        return $this->hasMany('App\Review');
    }

    function images() {
        return $this->hasMany('App\Image');
    }
}
